###### 玩转金刚梯>金刚字典>

### 搭梯
- 所谓<Strong> 搭梯 </Strong>是指启动运行VPN产品，[ 翻墙 ](/LadderFree/kkDictionary/OverTheWall.md)成功
  - 对[ 金刚App梯 ](/LadderFree/kkDictionary/KKLadderAPP.md)而言，所谓<Strong> 搭梯 </Strong>是指启动运行[ 金刚App梯 ](/LadderFree/kkDictionary/KKLadderAPP.md)，使安装并运行[ 金刚App梯 ](/LadderFree/kkDictionary/KKLadderAPP.md)的设备整体处于[ 翻墙 ](/LadderFree/kkDictionary/OverTheWall.md)成功状态
  - 对[ 金刚号梯  ](/LadderFree/kkDictionary/KKLadderKKID.md)而言，所谓<Strong> 搭梯 </Strong>是指[ <strong>连通金刚号</strong> ](/LadderFree/kkDictionary/KKIDsUsage.md)，使[ 连通金刚号 ](/LadderFree/kkDictionary/KKIDsUsage.md)的设备整体处于[ 翻墙 ](/LadderFree/kkDictionary/OverTheWall.md)成功状态


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
